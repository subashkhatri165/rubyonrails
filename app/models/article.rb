class Article < ApplicationRecord
    def change
        create_table :articles do |t|
            t.string :title
            t.text :text

            t.timestamps
         end
    end

has_many :comments    
validates :title, presence: true,
                  length: {maximum: 10, minimum: 2}
validates :text, presence: true,
                  length: {maximum: 255, minimum: 10}
  

end
